import { Sprite, Container } from "pixi.js";
import { TimelineMax, Power0 } from "gsap/all";
import { Howl, Howler } from "howler";

const W = innerWidth;
const H = innerHeight;

class Game {
  constructor(app, stage) {
    this.app = app;
    this.stage = stage;
    this.scene = null;
    this.sound = "On";
    this.loader = PIXI.loader;
    this.activeSound = 1;
    this.recLocked = false;
    Howler.mobileAutoEnable = true;
    this.sounds = {
      l1: new Howl({
        src: "audio/1.mp3",
        onend: () => {
          this.clear();
          setTimeout(() => {
            this.sounds.b2.play();
            this.recLocked = false;
            this.recBtn.interactive = true;
          }, 2000);
        }
      }),
      l2: new Howl({
        src: "audio/2.mp3",
        onend: () => {
          this.clear();
          setTimeout(() => {
            this.sounds.b3.play();
            this.recLocked = false;
            this.recBtn.interactive = true;
          }, 2000);
        }
      }),
      l3: new Howl({
        src: "audio/3.mp3",
        onend: () => {
          this.clear();
          this.finishGame();
          this.activeSound = 1;
          this.recLocked = false;
        }
      }),
      b1: new Howl({
        src: "audio/b1.mp3",
        onend: () => {}
      }),
      b2: new Howl({
        src: "audio/b3.mp3",
        onend: () => {}
      }),
      b3: new Howl({
        src: "audio/b2.mp3",
        onend: () => {}
      })
    };
    this.mic = null;
    this.count = 0;
    this.animateLosyash = this.animateLosyash.bind(this);
    this.clear = this.clear.bind(this);
    this.aTick = this.aTick.bind(this);
    this.setScene = this.setScene.bind(this);
    this.onLoadComplete = this.onLoadComplete.bind(this);
    this.audioContext = new (window.AudioContext ||
      window.webkitAudioContext)();
    this.analyser = this.audioContext.createAnalyser();
    this.dataArray = new Uint8Array(this.analyser.frequencyBinCount);

    for (var i = 10001; i < 10049; i++) {
      this.loader.add(`lframe${i}`, `images/losyash/лосяш${i}.png`);
    }
    for (let i = 0; i < 240; i++) {
      let substr;
      if (i < 100) {
        if (i < 10) {
          substr = "00" + i;
        } else {
          substr = "0" + i;
        }
      } else {
        substr = i;
      }
      this.loader.add(`p1_bees_${i}`, `images/bees/phase1/fly_${substr}.png`);
    }

    this.loader
      .add("intro", "./stages/intro/atlas.json")
      .add("rules", "./stages/rules/atlas.json")
      .add("game", "./stages/game/atlas.json")
      .add("final", "./stages/final/atlas.json")
      .add("final_bg", "./stages/final/final_bg.png")
      .add("bees_final_1", "./images/bees/1.png")
      .add("bees_final_2", "./images/bees/2.png")
      .add("bees_final_3", "./images/bees/3.png")
      .add("p2_bees_0", "./images/bees/phase2/waves_0.png")
      .add("p2_bees_1", "./images/bees/phase2/waves_1.png")
      .add("p2_bees_2", "./images/bees/phase2/waves_2.png")
      .add("p2_bees_3", "./images/bees/phase2/waves_3.png")
      .add("p2_bees_4", "./images/bees/phase2/waves_4.png")
      .add("p2_bees_5", "./images/bees/phase2/waves_5.png")
      .load(this.onLoadComplete.bind(this));

    this.loader.on("progress", (loader, res) => {
      document.querySelector(
        "#pb-wrapper div"
      ).textContent = `Загрузка ${Math.ceil(loader.progress)} %`;
    });
  }

  clear(cb) {
    clearInterval(this.losyashId);
    this.los.texture = this.loader.resources["lframe10035"].texture;
    cb && cb();
  }

  getMicrophone() {
    const handleMedia = handler => {
      this.mic = handler;
      this.source = this.audioContext.createMediaStreamSource(this.mic);
      this.source.connect(this.analyser);
      this.rafId = requestAnimationFrame(this.aTick);
    };
    if (!navigator.getUserMedia) {
      console.log("guess I suck");
    } else if (!navigator.mediaDevices) {
      navigator.getUserMedia(
        {
          audio: true
        },
        handleMedia
      );
    } else {
      navigator.mediaDevices
        .getUserMedia({
          audio: true
        })
        .then(handleMedia)
        .catch(err => console.log(err));
    }
  }

  aTick() {
    // this.analyser.getByteTimeDomainData(this.dataArray);
    // this.rafId = requestAnimationFrame(this.aTick);
  }

  finishGame() {
    this.setScene("final");
  }

  drawWave(g) {
    const width = W;
    const height = 100;
    var dataArray = new Uint8Array(this.analyser.fftSize);

    const draw = () => {
      this.waveId = requestAnimationFrame(draw);
      this.analyser.getByteTimeDomainData(dataArray);
      const sliceWidth = (width * 1.0) / this.analyser.fftSize;
      let x = 0;
      g.clear();
      for (var i = 0; i < this.analyser.fftSize; i++) {
        const y = ((dataArray[i] / 90.0) * height) / 2;

        g.lineStyle(2, 0xffffff);
        if (i == 0) g.moveTo(x, y);
        else g.lineTo(x, y);
        x += sliceWidth;
      }
      g.lineTo(width, 0);
    };

    draw();
  }

  animateLosyash() {
    let idx = 10001;

    function swap() {
      idx = idx + 1 > 10048 ? 10001 : idx + 1;
      this.los.texture = this.loader.resources[`lframe${idx}`].texture;
    }

    this.losyashId = setInterval(swap.bind(this), 1000 / 24);
  }

  stopMicrophone() {
    this.mic.getTracks().forEach(track => {
      track.stop();
    });
    this.mic = null;
    cancelAnimationFrame(this.rafId);
    this.analyser.disconnect();
    this.source.disconnect();
  }

  toggleMicrophone() {
    if (this.mic) {
      this.stopMicrophone();
    } else {
      this.getMicrophone();
    }
  }

  center(a, b) {
    return (a - b) / 2;
  }

  onLoadComplete() {
    const { stage } = this;

    this.scenes = {
      rules: this.createRules(),
      intro: this.createIntro(),
      game: this.createGame(),
      final: this.createFinish()
    };

    this.render(stage);
    document.querySelector("#pb-wrapper").style.display = "none";
  }

  fakeRecognition() {
    console.log(`Active snd : ${this.activeSound}, count: ${this.count}`);
    this.animateLosyash();
    this.sounds[`l${this.activeSound}`].play();
    if (this.activeSound === 3) return;
    this.activeSound++;
  }
  setScene(key) {
    const { scene, scenes } = this;
    if (scene) {
      this.app.stage.removeChild(scene);
    }
    window.stage = scenes[key];

    this.stage = key;
    this.app.stage.addChild(scenes[key]);
  }
  createRules() {
    const assets = this.loader.resources.rules.textures;
    let container = new Container();
    let bg = new Sprite(assets["BG_Rules.png"]);
    bg.width = W;
    bg.height = W > 350 ? H + 300 : H + 250;
    bg.y = -150;

    let btn = new Sprite(assets["BTN_Ok.png"]);
    btn.width = btn.width / 3;
    btn.height = btn.height / 3;
    btn.x = W - btn.width;
    btn.y = H - btn.height;
    btn.buttonMode = true;
    btn.interactive = true;
    btn.on("pointerdown", () => {
      btn.texture = assets["BTN_Ok_tap.png"];
    });
    btn.on("pointerup", () => {
      btn.texture = assets["BTN_Ok.png"];
      if (this.stage === "rules") {
        this.setScene("game");
        setTimeout(() => {
          this.sounds[`b${this.activeSound}`].play();
        }, 500);
      }
    });

    let close_btn = new Sprite(
      this.loader.resources.game.textures["BTN_Close.png"]
    );
    close_btn.width = 50;
    close_btn.height = 50;
    close_btn.x = W - close_btn.width;
    close_btn.y = 0;
    close_btn.buttonMode = true;
    close_btn.interactive = true;

    close_btn.on("pointerdown", () => {
      close_btn.texture = this.loader.resources.game.textures[
        "BTN_Close_tap.png"
      ];
    });

    close_btn.on("pointerup", () => {
      if (this.stage === "rules") {
        close_btn.texture = this.loader.resources.game.textures[
          "BTN_Close.png"
        ];
        ym(51873824, "reachGoal", "click_close");

        window.close();
      }
    });

    container.addChild(bg);
    container.addChild(btn);
    container.addChild(close_btn);
    return container;
  }
  createIntro() {
    const raysTml = new TimelineMax();
    const beesTml = new TimelineMax({ yoyo: true, repeat: -1 });

    const assets = this.loader.resources.intro.textures;

    let container = new Container();
    let bg = new Sprite(assets["BG.png"]);
    bg.width = W + 50;
    bg.height = bg.width * 2;

    if (W < 350) bg.y = -100;

    let bees = new Sprite(assets["Bee.png"]);
    bees.width = W * 1.15;
    bees.height = bees.width - 100;
    bees.x = this.center(W, bees.width);
    bees.y = 10;

    let losyash = new Sprite(assets["Losyash.png"]);
    losyash.width = W * 1.15;
    losyash.height = losyash.width;
    losyash.x = W - losyash.width + 70;
    losyash.y = H - losyash.height + 90;

    let logo = new Sprite(assets["Logo.png"]);
    logo.width = W / 1.5;
    logo.height = logo.width;
    logo.x = (W - logo.width) / 2;

    let playBtn = new Sprite(assets["BTN_Play.png"]);
    playBtn.width = 100;
    playBtn.height = 99;
    playBtn.x = this.center(W, playBtn.width);
    playBtn.y = this.center(H, playBtn.height);
    playBtn.interactive = true;
    playBtn.buttonMode = true;
    playBtn.on("pointerdown", () => {
      playBtn.texture = assets["BTN_Play_tap.png"];
    });
    playBtn.on("pointerup", () => {
      playBtn.texture = assets["BTN_Play.png"];
      if (this.stage === "intro") this.setScene("rules");
    });

    let rays = new Sprite(assets["Rays.png"]);
    rays.width = W * 3;
    rays.height = rays.width;
    rays.anchor.x = 0.5;
    rays.anchor.y = 0.5;
    rays.x = playBtn.x + playBtn.width / 2;
    rays.y = playBtn.y + playBtn.height / 2;

    raysTml.to(rays, 1000, { rotation: 360, repeat: -1 });
    beesTml.to(bees, 1, { y: bees.y - 10 });

    let close_btn = new Sprite(
      this.loader.resources.game.textures["BTN_Close.png"]
    );
    close_btn.width = 50;
    close_btn.height = 50;
    close_btn.x = W - close_btn.width;
    close_btn.y = 0;
    close_btn.buttonMode = true;
    close_btn.interactive = true;

    close_btn.on("pointerdown", () => {
      close_btn.texture = this.loader.resources.game.textures[
        "BTN_Close_tap.png"
      ];
    });

    close_btn.on("pointerup", () => {
      if (this.stage === "intro") {
        close_btn.texture = this.loader.resources.game.textures[
          "BTN_Close.png"
        ];
        ym(51873824, "reachGoal", "click_close");

        window.close();
      }
    });

    container.addChild(bg);
    container.addChild(rays);
    container.addChild(bees);
    container.addChild(losyash);
    container.addChild(logo);
    container.addChild(playBtn);
    container.addChild(close_btn);
    return container;
  }
  createGame() {
    Howler.mute(false);
    const assets = this.loader.resources.game.textures;
    let container = new Container();
    let bg = new Sprite(assets["BG.png"]);

    bg.width = W;
    bg.height = H + 200;
    bg.y = -100;

    let controlPanel = new Container();

    let panel = new Sprite(assets["Panel.png"]);
    let g = new PIXI.Graphics();

    let waveContainer = new Container();
    waveContainer.x = 0;
    waveContainer.y = 20;
    waveContainer.width = controlPanel.width;
    waveContainer.height = controlPanel.height / 2;

    waveContainer.addChild(g);
    g.position.set(0, -20);

    controlPanel.addChild(panel);
    controlPanel.addChild(waveContainer);

    panel.width = W;
    panel.height = panel.width / 1.9;

    controlPanel.y = H - controlPanel.height;

    let losyash = new Sprite(assets["Losyash.png"]);
    losyash.anchor.x = 1;
    losyash.scale.x *= -1;

    losyash.width = 200;
    losyash.height = 211;
    losyash.y = H - losyash.height - 230;
    losyash.x = -30;

    if (W > 320) {
      losyash.width = 300;
      losyash.height = 310;
      losyash.x = -70;
      losyash.y = H - losyash.height - 270;
    }
    this.los = losyash;

    let shadow = new Sprite(assets["Shadow_Losyash.png"]);

    shadow.width = W > 320 ? 260 : 160;
    shadow.height = W > 320 ? 90 : 47;
    shadow.x = losyash.x + 20;
    shadow.y = losyash.y + losyash.height - 30;

    let close = new Sprite(assets["BTN_Close.png"]);
    close.width = 50;
    close.height = 50;
    close.x = W - close.width;

    let rec = new Sprite(assets["BTN_Rec.png"]);
    rec.width = 100;
    rec.height = 100;
    rec.x = this.center(W, rec.width);
    rec.y = H - rec.height;
    this.recBtn = rec;

    let help = new Sprite(assets["BTN_Help.png"]);
    help.width = 50;
    help.height = 50;

    let sound = new Sprite(assets["BTN_SoundOn.png"]);
    sound.width = 50;
    sound.height = 50;
    sound.x = sound.width;

    let bubble_g = new Container();
    let bubble = new Sprite(assets["Bubble.png"]);
    bubble.width = 250;
    bubble.height = 88;
    bubble_g.addChild(bubble);
    bubble_g.x = losyash.x;
    bubble_g.y = losyash.y + losyash.height / 2 + bubble_g.height / 2;

    [rec, help, sound, close].map(btn => {
      btn.interactive = true;
      btn.buttonMode = true;
    });

    rec.on("pointerdown", () => {
      rec.texture = assets["BTN_Rec_tap.png"];
    });
    this.drawWave = this.drawWave.bind(this, g);
    g.alpha = 0;

    rec.on("pointerup", () => {
      if (this.stage === "game") {
        rec.interactive = false;
        rec.texture = assets["BTN_Rec.png"];
        if (this.recLocked) {
          console.log(`${Date.now()} Rec button locked`);
          return;
        }
        console.log(`${Date.now()}  Rec button unlocked`);
        this.toggleMicrophone();
        // this.drawWave();
        this.app.wave.container.style.opacity = 1;
        setTimeout(() => {
          this.toggleMicrophone();
          this.fakeRecognition();
          this.app.wave.container.style.opacity = 0;
        }, 3000);
        this.recLocked = true;
        ym(51873824, "reachGoal", "click_speak");
      }
    });

    rec.on("pointerleave", () => {
      rec.texture = assets["BTN_Rec.png"];
    });

    close.on("pointerdown", () => {
      close.texture = assets["BTN_Close_tap.png"];
    });

    close.on("pointerup", () => {
      if (this.stage === "game") {
        close.texture = assets["BTN_Close.png"];
        this.clear();
        this.setScene("intro");
        Object.values(this.sounds).map(snd => snd.stop());
        this.activeSound = 1;
        ym(51873824, "reachGoal", "click_close");
        window.close();
      }
    });

    close.on("pointerleave", () => {
      close.texture = this.assets["BTN_Close.png"];
    });

    help.on("pointerdown", () => {
      help.texture = assets["BTN_Help_tap.png"];
    });

    help.on("pointerup", () => {
      help.texture = assets["BTN_Help.png"];
      if (this.stage === "game") this.setScene("rules");
    });

    help.on("pointerleave", () => {
      help.texture = assets["BTN_Help.png"];
    });

    sound.on("pointerdown", () => {
      sound.texture = assets[`BTN_Sound${this.sound}_tap.png`];
    });

    sound.on("pointerup", () => {
      if (this.stage === "game") {
        this.sound = this.sound === "On" ? "Off" : "On";
        const muted = this.sound === "Off";
        Howler.mute(muted);
        sound.texture = assets[`BTN_Sound${this.sound}.png`];
      }
    });

    container.addChild(bg);
    container.addChild(shadow);
    container.addChild(losyash);
    container.addChild(close);
    container.addChild(help);
    container.addChild(sound);
    container.addChild(controlPanel);
    container.addChild(rec);

    return container;
  }
  createFinish() {
    const assets = this.loader.resources.final.textures;

    let container = new Container();

    let bg = new PIXI.Sprite(this.loader.resources["final_bg"].texture);
    bg.width = W;
    bg.height = H;

    const beesAnim = (start, end, path, loop) => {
      var i = start;
      var int;
      return new Promise(resolve => {
        const swap = () => {
          if (this.stage !== "final") return;
          i++;

          if (i > end && !loop) {
            clearInterval(int);
            resolve();
          } else if (i >= end && loop) {
            i = start;
          }
          try{
            bees.texture = this.loader.resources[`${path}${i}`].texture;
          } catch(err){
              console.log("It's an ios hack cuz It reloads on exceptions and I'm a bad developer");
          }
        };
        int = setInterval(swap, 1000 / 60);
      });
    };

    let bees = new PIXI.Sprite(this.loader.resources["p1_bees_0"].texture);
    bees.width = W;
    bees.height = bees.width;
    bees.x = 0;
    bees.y = this.center(H, bees.height);
    beesAnim(60, 239, "p1_bees_").then(() => {
      beesAnim(0, 5, "p2_bees_", true);
    });

    let btn = new PIXI.Sprite(assets["BTN_Selfie.png"]);
    btn.interactive = btn.buttonMode = true;
    btn.width = 150;
    btn.height = 72;
    btn.x = this.center(W, btn.width);
    btn.y = H - btn.height;

    let close_btn = new Sprite(
      this.loader.resources.game.textures["BTN_Close.png"]
    );
    close_btn.width = 50;
    close_btn.height = 50;
    close_btn.x = W - close_btn.width;
    close_btn.y = 0;
    close_btn.buttonMode = true;
    close_btn.interactive = true;

    close_btn.on("pointerdown", () => {
      close_btn.texture = this.loader.resources.game.textures[
        "BTN_Close_tap.png"
      ];
    });

    close_btn.on("pointerup", () => {
      if (this.stage === "final") {
        close_btn.texture = this.loader.resources.game.textures[
          "BTN_Close.png"
        ];
        this.clear();
        this.setScene("intro");
        Object.values(this.sounds).map(snd => snd.stop());
        this.activeSound = 1;
        ym(51873824, "reachGoal", "click_close");
        window.close();
      }
    });

    btn.on("pointerdown", () => {
      btn.texture = assets["BTN_Selfie_tap.png"];
    });

    btn.on("pointerup", () => {
      if (this.stage === "final") {
        btn.texture = assets["BTN_Selfie.png"];
        ym(51873824, "reachGoal", "click_selfie");
        location.href = "shazam://openzap?zid=SkHi1c&campaign=abc";
      }
    });

    container.addChild(bg);
    container.addChild(bees);
    container.addChild(btn);
    container.addChild(close_btn);

    return container;
  }

  render() {
    const { stage } = this;
    this.setScene(stage);
  }
}

export default Game;
