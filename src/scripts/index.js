import "../styles/index.scss";
import * as PIXI from "pixi.js";
import Game from "./Game";
import SiriWave from "siriwave";

PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.LINEAR;

const root = document.querySelector("#root");
const three = document.querySelector("#three");

const width = innerWidth > 414 ? 414 : innerWidth;

const app = new PIXI.Application({
  width: width,
  height: innerHeight,
  resolution: devicePixelRatio,
  autoResize: true
});
const game = new Game(app, "intro");
localStorage.setItem("local storage", true);
const wave = new SiriWave({
  container: document.querySelector("#wave"),
  width: innerWidth > 414 ? 414 : innerWidth,
  height: 300,
  style: "ios9"
});
app.wave = wave;
wave.start();

root.appendChild(app.view);
